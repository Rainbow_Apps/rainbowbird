//
//  Block.swift
//  RainbowBird
//
//  Created by Yukinaga Azuma on 2014/10/23.
//  Copyright (c) 2014年 Yukinaga Azuma. All rights reserved.
//

import Foundation
import UIKit

class Block: UIImageView{
    
    var xSpeed:CGFloat = -1.0
    let colorArray = [UIColor.redColor(), UIColor.blueColor(), UIColor.brownColor(), UIColor.orangeColor(), UIColor.yellowColor(), UIColor.purpleColor(), UIColor.greenColor(), UIColor.grayColor(), UIColor.whiteColor()]
    
    func setColor(){
        let rand:UInt32 = arc4random() % (UInt32)(colorArray.count)
        self.layer.borderColor = colorArray[(Int)(rand)].CGColor!
//        self.backgroundColor = UIColor.whiteColor()
    }
    
}