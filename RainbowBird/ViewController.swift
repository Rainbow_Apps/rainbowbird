//
//  ViewController.swift
//  RainbowBird
//
//  Created by Yukinaga Azuma on 2014/10/23.
//  Copyright (c) 2014年 Yukinaga Azuma. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    //ゲーム画面
    @IBOutlet var gameView:UIView!
    //主人公
    @IBOutlet var bird:Bird!
    //タイトルのラベル
    @IBOutlet var titleLabel:UILabel!
    //Tap to Startのラベル
    @IBOutlet var startLabel:UILabel!
    //画面を覆う透明のボタン
    @IBOutlet var screenButton:UIButton!
    //距離のラベル
    @IBOutlet var distanceLabel:UILabel!
    //ハイスコアラベル
    @IBOutlet var highScoreLabel:UILabel!
    
    //単位サイズ
    var unitSize:CGFloat?
    //タイマー処理
    var timer:NSTimer?
    //ゲーム中かどうかの判定フラグ
    var isGaming:Bool = false
    //ブロックを格納する配列
    var blockArray:[Block] = []
    //ブロック出現時間までのカウント
    var timerCount = 0
    //距離更新のカウント
    var distanceCount = 0
    //ブロック間の距離
    var holeHeight:CGFloat = 120.0
    //効果音を扱う
    var seManager:SEManager = SEManager()
    //BGMを扱う
    var bgmManager:BGMManager = BGMManager()
    
    //画面が読み込まれた直後に実行される
    override func viewDidLoad() {
        super.viewDidLoad()
        //ゲーム画面を端末のサイズに合わせて設定
        gameViewSetting()
        //ハイスコアの読み込み
        showHighScore()
        //BGMの再生
        bgmManager.play(soundName: "titleBGM.mp3")
        //鳥のはばたき
        /*
        let image1 = UIImage(named: "hero1.png")!
        let image2 = UIImage(named: "hero2.png")!
        let image3 = UIImage(named: "hero3.png")!
        var birdImageArray:Array = [image1, image2, image3, image2]
        bird.animationDuration = 0.5
        bird.animationImages = birdImageArray
        bird.startAnimating()*/
    }
    
    //画面タップ時の処理
    @IBAction func screenTapped() {
        //ゲームプレイ時の場合
        if isGaming {
            //鳥のスピードを上向きに
            bird.ySpeed = adjust(-4.0)
            //効果音を再生
            seManager.play(soundName:"tap.mp3")
        //タイトル画面の場合
        }else{
            //タイトルラベルを隠す
            titleLabel.hidden = true
            //Tap to Startを隠す
            startLabel.hidden = true
            //ゲーム中かどうかの判定フラグをオンに
            isGaming = true
            //0.01ごとにdoActionメソッドを呼び出す
            timer = NSTimer.scheduledTimerWithTimeInterval(0.01,
                target: self,
                selector: Selector("doAction"),
                userInfo: nil,
                repeats: true)
            //BGMの再生
            bgmManager.play(soundName: "gameBGM.mp3")
        }
    }
    
    //0.01秒ごとに呼び出されるメソッド
    func doAction(){
        //鳥のスピードを下方向に増加
        bird.ySpeed += adjust(0.1)
        //鳥のy座標をスピード分だけ移動
        bird.center = CGPointMake(bird.center.x, bird.center.y+bird.ySpeed)
        //カウントを増加
        timerCount++
        //カウントが200に達したら
        if timerCount >= 200{
            //ブロックを発生させる
            generateBlocks()
            //カウントを0に戻す
            timerCount = 0
        }
        
        //距離カウントを増加
        distanceCount++
        //距離カウントが10に達したら
        if distanceCount >= 10{
            //距離ラベルの更新
            var meter:Int! = distanceLabel.text?.toInt()
            distanceLabel.text = "\(meter+1)"
            //距離カウントを0に戻す
            distanceCount = 0
        }
        
        //配列のインデックス
        var i:Int = 0
        //配列の全ての要素に対して処理を行う
        while i<blockArray.count{
            //配列の各要素を取り出す
            var block:Block = blockArray[i]
            
            //当たり判定 ブロックと鳥が近かったら
            if abs(block.center.x-bird.center.x) < block.frame.size.width/2.0+bird.frame.size.width/4.0 && abs(block.center.y-bird.center.y) < block.frame.size.height/2.0{
                //ゲームオーバー時の処理
                gameOver()
                //メソッドを終了
                return
            }
            
            //ブロックを移動
            block.center = CGPointMake(block.center.x+adjust(block.xSpeed), block.center.y)
            //ブロックが画面外に出たら
            if block.center.x < 0-block.frame.size.width/2.0{
                //ブロックを画面から消去
                block.removeFromSuperview()
                //ブロックを配列から消去
                blockArray.removeAtIndex(i)
            }else{
                //次のインデックスへ
                i++
            }
        }
        
        //鳥が地面についたら
        if bird.center.y+bird.frame.size.height/2.0 > gameView.frame.size.height{
            //ゲームオーバー時の処理
            gameOver()
            //メソッドを終了
            return
        }
    }
    
    //ブロックの発生
    func generateBlocks(){
        //ブロックの幅
        var blockWidth:CGFloat = 30.0
        //ブロックの高さ
        var blockHeight:CGFloat = 480.0-holeHeight
        //乱数
        let randY:UInt32 = arc4random()%151
        //ブロックの隙間をランダムに移動する距離
        var bias:CGFloat = (CGFloat)(randY)
        bias -= 75.0
        //ブロックを上下に2つ発生させる
        for i in 0...1{
            //ブロックを生成
            var block:Block! = Block()
            //ブロックの幅、高さを指定
            block.frame = CGRectMake(0, 0,
                adjust(blockWidth), adjust(blockHeight))
            //ブロックの中央座標を指定
            block.center = CGPointMake(adjust(320.0+blockWidth/2.0), adjust((CGFloat)(i)*480.0+bias))
            //ブロックの色を設定
            block.setColor()
            //ブロックを半透明に設定
//            block.alpha = 0.7
            //ブロックを角丸に
            block.layer.cornerRadius = adjust(blockWidth/2.0)
            //ブロックのボーダーの幅
            block.layer.borderWidth = 10.0
            //画面にブロックを追加
            gameView.addSubview(block)
            //配列にブロックを追加
            blockArray.append(block)
        }
        
        //ブロック間の距離を少しだけ狭くする
        holeHeight -= 2.0
        if holeHeight < 30{
            holeHeight = 30.0
        }
        
        //距離ラベル、ハイスコアラベルを手前に
        gameView.bringSubviewToFront(distanceLabel)
        gameView.bringSubviewToFront(highScoreLabel)
    }
    
    //タイトル画面に戻る
    func setTitle(){
        //画面タップを有効に
        screenButton.enabled = true
        //タイトルラベルを表示
        titleLabel.hidden = false
        //Tap to Startを表示
        startLabel.hidden = false
        //ゲームプレイ中フラグをオフに
        isGaming = false
        //鳥の中央座標
        bird.center = CGPointMake(bird.center.x, adjust(178))
        //鳥のスピード
        bird.ySpeed = 0.0
        //鳥を変形前の状態に
        bird.transform = CGAffineTransformIdentity
        //距離を0に
        distanceLabel.text = "0"
        //ブロック間の距離を初期値に
        holeHeight = 120.0
        //全てのブロックに対して
        for block in blockArray{
            //ブロックを画面から削除
            block.removeFromSuperview()
        }
        //配列から全てのブロックを削除
        blockArray.removeAll()
        //羽ばたきの開始
        bird.startAnimating()
        //ハイスコアの読み込み
        showHighScore()
        //BGMの再生
        bgmManager.play(soundName: "titleBGM.mp3")
    }
    
    //ゲームオーバーの処理
    func gameOver(){
        //タイマーの停止
        timer?.invalidate()
        //鳥を画面の一番手前に
        gameView.bringSubviewToFront(bird)
        //画面タップを無効に
        screenButton.enabled = false
        //鳥が落ちる位置
        let fallYPosition:CGFloat = gameView.frame.size.height-bird.frame.size.width/2.0
        //アニメーションの所要時間
        let duration:NSTimeInterval = (NSTimeInterval)((fallYPosition-bird.center.y)/adjust(240.0))

        //アニメーションの実行
        UIView.animateWithDuration(duration, animations: { () -> Void in
            //アニメーション後の鳥の中央座標
            self.bird.center = CGPointMake(self.bird.center.x, fallYPosition)
            //鳥を180°回転するアニメーション
            self.bird.transform = CGAffineTransformMakeRotation((CGFloat)(M_PI))
            }){ (Bool) -> Void in
                //アニメーションの終了後の処理
                self.waitUntilTitle()
        }
        
        //羽ばたきの停止
        bird.stopAnimating()
        //ハイスコアの保存
        saveHighScore()
        //効果音の再生
        seManager.play(soundName:"gameOver.mp3")
    }
    
    //一定時間後にタイトル画面へ
    func waitUntilTitle(){
        //2秒後にsetTitleメソッドを実行
        NSTimer.scheduledTimerWithTimeInterval(2.0,
            target: self,
            selector: Selector("setTitle"),
            userInfo: nil,
            repeats: false)
    }
    
    //ハイスコアの表示
    func showHighScore(){
        let uD = NSUserDefaults.standardUserDefaults()
        let highScore:Int = uD.integerForKey("HighScore")
        highScoreLabel.text = "High score:\(highScore)m"
    }
    
    //ハイスコアの保存
    func saveHighScore(){
        let uD = NSUserDefaults.standardUserDefaults()
        var highScore:Int = uD.integerForKey("HighScore")
        let score:Int! = distanceLabel.text?.toInt()
        if score > highScore {
            highScore = score
            uD.setInteger(highScore, forKey:"HighScore")
        }
    }
    
    //機種ごとの画面サイズの違いに対応
    func gameViewSetting(){
        //ゲーム画面の幅
        let gameViewWidth = self.view.frame.size.width;
        //ゲーム画面の高さ
        let gameViewHeight = self.view.frame.size.width/gameView.frame.size.width*gameView.frame.size.height
        //ゲーム画面のY座標
        let gameViewYPosition = (self.view.frame.size.height-gameViewHeight)/2.0
        //ゲーム画面の位置、サイズを指定
        gameView.frame = CGRectMake(0,gameViewYPosition,gameViewWidth,gameViewHeight)
        //単位サイズ
        unitSize = gameViewWidth/320.0
    }
    
    //座標をユニバーサルなものに変換
    func adjust(position: CGFloat)->CGFloat{
        return position*unitSize!
    }
    
    //メモリ不足時の処理
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}