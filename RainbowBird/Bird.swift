//
//  Bird.swift
//  RainbowBird
//
//  Created by Yukinaga Azuma on 2014/10/23.
//  Copyright (c) 2014年 Yukinaga Azuma. All rights reserved.
//

import Foundation
import UIKit

class Bird: UIImageView{
    
    var delegate:BirdDelegate? = nil
    
    var ySpeed:CGFloat = 0.0
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        delegate?.birdTapped()
    }
    
}

protocol BirdDelegate {
    func birdTapped()
}