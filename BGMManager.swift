//
//  BGMManager.swift
//  5-1
//
//  Created by Yukinaga Azuma on 2014/10/10.
//  Copyright (c) 2014年 Yukinaga Azuma. All rights reserved.
//

import Foundation
import AVFoundation

class BGMManager:NSObject, AVAudioPlayerDelegate {
    
    var player:AVAudioPlayer? = nil
    var soundVolume = 1.0
    
    func play(soundName sName:String){
        let soundPath:String = NSBundle.mainBundle().bundlePath.stringByAppendingPathComponent(sName)
        let url:NSURL? = NSURL.fileURLWithPath(soundPath)
        player?.stop()
        player = nil
        player = AVAudioPlayer(contentsOfURL:url, error:nil)
        if let player = player{
            player.numberOfLoops = -1
            player.delegate = self
            player.prepareToPlay()
            player.play()
        }
    }
    
    func stop() {
        player?.stop()
    }
    
    func resume() {
        player?.play()
    }
}
